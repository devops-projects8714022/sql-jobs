pipeline {
    agent any

    environment {
        DB_HOST = 'localhost'
        DB_USER = 'your_username'
        DB_PASS = 'your_password'
        DB_NAME = 'your_database'
        EXCLUDED_TABLE = 'table_to_exclude'
        BACKUP_DIR = '/tmp/mysql_backup'
        DATE = sh(script: 'date +%Y%m%d_%H%M%S', returnStdout: true).trim()
        BACKUP_FILE = "${env.BACKUP_DIR}/${env.DB_NAME}_backup_${env.DATE}.sql"
        S3_BUCKET = 's3://your-bucket-name'
    }

    triggers {
        cron('0 2 * * *')
    }

    stages {
        stage('Backup Database') {
            steps {
                sh '''
                #!/bin/bash
                mkdir -p ${BACKUP_DIR}
                mysqldump -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} --ignore-table=${DB_NAME}.${EXCLUDED_TABLE} ${DB_NAME} > ${BACKUP_FILE}

                if [ $? -eq 0 ]; then
                  echo "Backup of ${DB_NAME} excluding ${EXCLUDED_TABLE} was successful. Backup file: ${BACKUP_FILE}"
                  aws s3 cp ${BACKUP_FILE} ${S3_BUCKET}

                  if [ $? -eq 0 ]; then
                    echo "Backup file uploaded to S3 bucket successfully."
                    rm ${BACKUP_FILE}
                  else
                    echo "Failed to upload backup file to S3 bucket."
                  fi
                else
                  echo "Backup of ${DB_NAME} excluding ${EXCLUDED_TABLE} failed."
                fi
                '''
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
