## Создание бэкапа базы данных MySQL и загрузка в AWS S3

Этот Jenkins Pipeline скрипт предназначен для создания бэкапа базы данных MySQL (исключая определённую таблицу) и его загрузки в указанный AWS S3 bucket. Скрипт выполняет следующие шаги:

1. **Создание бэкапа**: Создание дампа базы данных MySQL с исключением указанной таблицы.
2. **Загрузка бэкапа в S3**: Загрузка созданного дампа в указанный S3 bucket.
3. **Очистка**: Удаление локального файла бэкапа после успешной загрузки в S3.

### Настройка

Перед использованием этого скрипта, убедитесь, что:

- Jenkins установлен и настроен.
- Установлены необходимые плагины (например, Pipeline).
- На Jenkins агенте установлен и настроен AWS CLI.
- В Jenkins настроены учетные данные для доступа к базе данных MySQL и S3 bucket.

### Pipeline скрипт

Добавьте следующий скрипт в ваш Pipeline job в Jenkins:

```groovy
pipeline {
    agent any

    environment {
        DB_HOST = 'localhost'
        DB_USER = 'your_username'
        DB_PASS = 'your_password'
        DB_NAME = 'your_database'
        EXCLUDED_TABLE = 'table_to_exclude'
        BACKUP_DIR = '/tmp/mysql_backup'
        DATE = sh(script: 'date +%Y%m%d_%H%M%S', returnStdout: true).trim()
        BACKUP_FILE = "${env.BACKUP_DIR}/${env.DB_NAME}_backup_${env.DATE}.sql"
        S3_BUCKET = 's3://your-bucket-name'
    }

    triggers {
        cron('0 2 * * *')
    }

    stages {
        stage('Backup Database') {
            steps {
                sh '''
                # Ensure the backup directory exists
                mkdir -p ${BACKUP_DIR}

                # Create a backup of the database excluding the specified table
                mysqldump -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} --ignore-table=${DB_NAME}.${EXCLUDED_TABLE} ${DB_NAME} > ${BACKUP_FILE}

                if [ $? -eq 0 ]; then
                  echo "Backup of ${DB_NAME} excluding ${EXCLUDED_TABLE} was successful. Backup file: ${BACKUP_FILE}"
                  
                  # Upload the backup file to the specified S3 bucket
                  aws s3 cp ${BACKUP_FILE} ${S3_BUCKET}

                  if [ $? -eq 0 ]; then
                    echo "Backup file uploaded to S3 bucket successfully."
                    # Optionally, remove the local backup file after successful upload
                    rm ${BACKUP_FILE}
                  else
                    echo "Failed to upload backup file to S3 bucket."
                  fi
                else
                  echo "Backup of ${DB_NAME} excluding ${EXCLUDED_TABLE} failed."
                fi
                '''
            }
        }
    }

    post {
        cleanup {
            // Clean up the workspace
            cleanWs()
        }
    }
}
```

### Переменные окружения

- `DB_HOST`: Хост базы данных MySQL.
- `DB_USER`: Имя пользователя базы данных MySQL.
- `DB_PASS`: Пароль пользователя базы данных MySQL.
- `DB_NAME`: Имя базы данных MySQL.
- `EXCLUDED_TABLE`: Имя таблицы, которую нужно исключить из бэкапа.
- `BACKUP_DIR`: Локальная директория на Jenkins агенте для хранения бэкапов.
- `DATE`: Текущая дата и время в формате `YYYYMMDD_HHMMSS`.
- `BACKUP_FILE`: Полный путь к создаваемому файлу бэкапа.
- `S3_BUCKET`: Путь к S3 bucket, куда будет загружен бэкап.

### Крон расписание

Добавьте триггер для регулярного запуска джоба, например, для ежедневного запуска в 02:00, используйте следующее расписание в разделе Triggers:

```groovy
triggers {
    cron('0 2 * * *')
}
```

### Примечание

Убедитесь, что у вас есть необходимые права доступа для записи файлов в S3 bucket и чтения из базы данных MySQL.